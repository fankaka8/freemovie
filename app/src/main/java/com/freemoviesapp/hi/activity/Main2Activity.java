package com.freemoviesapp.hi.activity;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.freemoviesapp.hi.R;
import com.freemoviesapp.hi.adapter.MoviesAdapter;
import com.freemoviesapp.hi.adapter.MoviesGridAdapter;
import com.freemoviesapp.hi.adapter.TitleNavigationAdapter;
import com.freemoviesapp.hi.data.MoviesDBHelper;
import com.freemoviesapp.hi.data.MoviesDao;
import com.freemoviesapp.hi.data.MoviesFromJSONExtractor;
import com.freemoviesapp.hi.model.Movie;
import com.freemoviesapp.hi.model.SpinnerNavItem;
import com.freemoviesapp.hi.network.ApiClient;
import com.freemoviesapp.hi.utils.NetworkHelper;
import com.freemoviesapp.hi.utils.TypeMoviesPref;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ApiClient.ResponseCallback, SearchView.OnQueryTextListener {

    private ConstraintLayout waitNotification;
    private ConstraintLayout waitForNewDataNotification;
    private RecyclerView moviesView;
    private MoviesAdapter adapter;
    private MoviesGridAdapter adapterGrid;
    private LinearLayoutManager layoutManager;
    private GridLayoutManager layoutGridManager;
    private boolean loading = true;
    private boolean useGrid = !true;

    private ApiClient client;
    private MoviesFromJSONExtractor moviesExtractor;
    private MoviesDao moviesDao;

    private FirebaseAnalytics firebaseAnalytics;

    // Title navigation Spinner data
    private ArrayList<SpinnerNavItem> navSpinner;

    // Navigation adapter
    private TitleNavigationAdapter adapterNavi;

    //
    private TypeMoviesPref typeMoviesPref;
    private int itemTypeMoviesSelected = 0;

    // search
    private SearchView mSearchView;
    public static String textSearch = "";

    public void useGetData(int itemTypeMoviesSelected) {
        if (itemTypeMoviesSelected == 0) {
            client.getData(Main2Activity.this);
        } else if (itemTypeMoviesSelected == 1) {
            client.getAcademicData(Main2Activity.this);
        } else if (itemTypeMoviesSelected == 2) {
            client.getEphmeraData(Main2Activity.this);
        } else if (itemTypeMoviesSelected == 3) {
            client.getTelevisionData(Main2Activity.this);
        } else if (itemTypeMoviesSelected == 4) {
            client.getHorrorData(Main2Activity.this);
        } else if (itemTypeMoviesSelected == 5) {
            client.getSilentData(Main2Activity.this);
        } else if (itemTypeMoviesSelected == 6) {
            client.getHomeData(Main2Activity.this);
        } else if (itemTypeMoviesSelected == 7) {
            client.getSearchData(Main2Activity.this, textSearch);
        }
    }

    public void setTitleForBar() {
        if (itemTypeMoviesSelected == 0) {
            getSupportActionBar().setTitle("Free Movies");
        } else if (itemTypeMoviesSelected == 1) {
            getSupportActionBar().setTitle("Academic Films");
        } else if (itemTypeMoviesSelected == 2) {
            getSupportActionBar().setTitle("Ephemeral Films");
        } else if (itemTypeMoviesSelected == 3) {
            getSupportActionBar().setTitle("Television");
        } else if (itemTypeMoviesSelected == 4) {
            getSupportActionBar().setTitle("Horror Films");
        } else if (itemTypeMoviesSelected == 5) {
            getSupportActionBar().setTitle("Silent Films");
        } else if (itemTypeMoviesSelected == 6) {
            getSupportActionBar().setTitle("Home Movies");
        } else if (itemTypeMoviesSelected == 7 && textSearch.length() > 0) {
            getSupportActionBar().setTitle(textSearch);
        } else {
            getSupportActionBar().setTitle("Free Movies");
        }
    }

    public void useGridLayout(boolean useGridLayout) {
        if (useGridLayout) {
            moviesView.setLayoutManager(layoutGridManager);
            moviesView.setHasFixedSize(true);
            moviesView.setAdapter(adapterGrid);
        } else {
            moviesView.setLayoutManager(layoutManager);
            moviesView.setAdapter(adapter);
        }
    }

    private int moviesToLoad = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        MobileAds.initialize(this, getBaseContext().getResources().getString(R.string.ad_unit_id));

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("73EC02899E0DE80F79B9298CA568790D").build();
        mAdView.loadAd(adRequest);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        typeMoviesPref = new TypeMoviesPref(getBaseContext());
        itemTypeMoviesSelected = typeMoviesPref.getNumberType();

        setTitleForBar();

        if(!NetworkHelper.isNetworkAvailable(getBaseContext())) {
            findViewById(R.id.layoutNoInternet).setVisibility(View.VISIBLE);
            findViewById(R.id.activity_main).setVisibility(View.GONE);
        } else {
            findViewById(R.id.layoutNoInternet).setVisibility(View.GONE);
            findViewById(R.id.activity_main).setVisibility(View.VISIBLE);
        }

        findViewById(R.id.bntRefresh).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(getIntent());
            }
        });

        client = new ApiClient();
//        client.stopAsynctaskRunning();
        moviesExtractor = new MoviesFromJSONExtractor();
        moviesDao = new MoviesDBHelper(this, MoviesDBHelper.VERSION);

        waitNotification = (ConstraintLayout) findViewById(R.id.retrievingDataNotification);
        waitForNewDataNotification = (ConstraintLayout) findViewById(R.id.fetchingNewData);

        moviesView = (RecyclerView) findViewById(R.id.moviesList);
        moviesView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (moviesToLoad == 0 && dy > 0) {
                    if (!useGrid) {
                        int visibleItemCount = layoutManager.getChildCount();
                        int totalItemCount = layoutManager.getItemCount();
                        int pastVisibleItems = layoutManager.findFirstVisibleItemPosition();
                        if (loading) {
                            if (visibleItemCount + pastVisibleItems >= totalItemCount) {
                                loading = false;
                                waitForNewDataNotification.setVisibility(View.VISIBLE);
//                                client.getData(MainActivity.this);
                                useGetData(itemTypeMoviesSelected);
                            }
                        }
                    } else {
                        int visibleItemCount = layoutGridManager.getChildCount();
                        int totalItemCount = layoutGridManager.getItemCount();
                        int pastVisibleItems = layoutGridManager.findFirstVisibleItemPosition();
                        if (loading) {
                            if (visibleItemCount + pastVisibleItems >= totalItemCount) {
                                loading = false;
                                waitForNewDataNotification.setVisibility(View.VISIBLE);
//                                client.getData(MainActivity.this);
                                useGetData(itemTypeMoviesSelected);
                            }
                        }
                    }
                }
            }
        });

        layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutGridManager = new GridLayoutManager(Main2Activity.this, 2);
        layoutGridManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position % 7 == 0) {
                    return 2;
                }

                return 1;
            }
        });
        moviesView.setLayoutManager(layoutManager);
        moviesView.setItemAnimator(new DefaultItemAnimator());
        adapter = new MoviesAdapter(this);
        adapterGrid = new MoviesGridAdapter(this);
        moviesView.setAdapter(adapter);

//        client.getData(this);
        useGetData(itemTypeMoviesSelected);

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        firebaseAnalytics.setMinimumSessionDuration(5000);
        firebaseAnalytics.setSessionTimeoutDuration(1000000);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onSuccess(String responseBody) {
        waitNotification.setVisibility(View.GONE);
        waitForNewDataNotification.setVisibility(View.VISIBLE);
        loading = true;
        moviesView.setVisibility(View.VISIBLE);

        try {
            Log.d("kaka Success", responseBody);
            extractMoviesFromResponse(responseBody);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Exception e) {
        waitNotification.setVisibility(View.GONE);
        waitForNewDataNotification.setVisibility(View.GONE);
        loading = true;
        e.printStackTrace();
    }

    private void extractMoviesFromResponse(String responseBody) throws JSONException {
        ArrayList<Movie> movies;
        movies = moviesExtractor.parse(responseBody);
        moviesToLoad = movies.size();

        for (Movie movie : movies) {
            Movie savedMovie = moviesDao.findByIdentifier(movie.getIdentifier(), itemTypeMoviesSelected);
            if (savedMovie == null) {
                fetchMovieUrls(movie);
            } else {
                adapter.add(savedMovie);
                adapter.notifyDataSetChanged();
                adapterGrid.add(savedMovie);
                adapterGrid.notifyDataSetChanged();
                if (--moviesToLoad == 0) {
                    waitForNewDataNotification.setVisibility(View.GONE);
                }
            }
        }
    }

    private void fetchMovieUrls(final Movie movie) {
        client.getFilesData(movie.getIdentifier(), new ApiClient.ResponseCallback() {
            @Override
            public void onSuccess(String responseBody) {
                try {
                    getUrlsFromXml(responseBody);
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
            }

            private void getUrlsFromXml(String responseBody) throws XmlPullParserException, IOException {
                ArrayList<String> fileNames = parseFiles(responseBody);
                getThumbnailUrl(fileNames);
                getVideoUrl(fileNames);
//                getDes(ApiClient.ARCHIVE_DETAILS + movie.getIdentifier());
                adapter.add(movie);
                adapter.notifyDataSetChanged();
                adapterGrid.add(movie);
                adapterGrid.notifyDataSetChanged();
                if(itemTypeMoviesSelected < 7) {
                    moviesDao.add(movie, itemTypeMoviesSelected);
                }
                if (--moviesToLoad == 0) {
                    waitForNewDataNotification.setVisibility(View.GONE);
                }
            }

            private void getVideoUrl(ArrayList<String> fileNames) {
                for (String filename : fileNames) {
                    String extension = filename.substring(filename.lastIndexOf('.') + 1, filename.length());
                    if (extension.equals("mp4")) {
                        movie.setVideoUrl(filename);
                        // [START screen_view_hit]
//                        Log.i("MainActivity", "Video FILE: " + filename);
//                        mTracker.setScreenName("Video FILE" + filename);
//                        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
                        // [END screen_view_hit]
                        break;
                    }
                }
            }

            private void getThumbnailUrl(ArrayList<String> fileNames) {
                for (String filename : fileNames) {
                    String extension = filename.substring(filename.lastIndexOf('.') + 1, filename.length());
                    if (extension.equals("jpg") || extension.equals("png")) {
                        // [START screen_view_hit]
//                        Log.i("MainActivity", "Image FILE: " + filename);
//                        mTracker.setScreenName("Image FILE" + filename);
//                        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
                        // [END screen_view_hit]
                        movie.setThumbnailUrl(filename);
                        break;
                    }
                }
            }

            private void getDes(String url) {
            }
        });
    }

    private ArrayList<String> parseFiles(String xml) throws XmlPullParserException, IOException {
        ArrayList<String> filenames = new ArrayList<>();

        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser parser = factory.newPullParser();
        parser.setInput(new StringReader(xml));
        int eventType = parser.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                if (parser != null && "file".equals(parser.getName())) {
                    filenames.add(parser.getAttributeValue(0));
                }
            }
            try {
                eventType = parser.next();
            } catch (Exception ignored) {
            }
        }
        return filenames;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_setting, menu);

        // Associate searchable configuration with the SearchView
        /*SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchView = (SearchView) menu.findItem(R.id.action_Search)
                .getActionView();
        mSearchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));*/

        MenuItem searchItem = menu.findItem(R.id.action_Search);
        mSearchView = (SearchView) searchItem.getActionView();
        setupSearchView(searchItem);

        ImageView closeButton = (ImageView)mSearchView.findViewById(R.id.search_close_btn);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!useGrid) {
                    adapter.filter("");
                } else {
                    adapterGrid.filter("");
                }
            }
        });

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                Log.d("onQuery", "onMenuItemActionCollapse");
                /*if(!useGrid) {
                    adapter.filter("");
                } else {
                    adapterGrid.filter("");
                }*/

                itemTypeMoviesSelected = 7;
                finish();
                startActivity(getIntent());

                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                Log.d("onQuery", "onMenuItemActionExpand");
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_GirdView:
                // change to gridview
                useGridLayout(true);
                useGrid = true;
                return true;
            case R.id.action_ListView:
                // change to listview
                useGridLayout(false);
                useGrid = false;
                return true;
            case R.id.action_SORTBY:
                // sort by
                useGridLayout(useGrid);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            minimizeApp();
            super.onBackPressed();
        }
    }

    public void minimizeApp() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }

    private void setupSearchView(MenuItem searchItem) {

        if (isAlwaysExpanded()) {
            mSearchView.setIconifiedByDefault(false);
        } else {
            searchItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM
                    | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        }

        if(mSearchView == null) {
            mSearchView = (SearchView) searchItem.getActionView();
        }
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if (searchManager != null) {
            List<SearchableInfo> searchables = searchManager.getSearchablesInGlobalSearch();

            SearchableInfo info = searchManager.getSearchableInfo(getComponentName());
            for (SearchableInfo inf : searchables) {
                if (inf.getSuggestAuthority() != null
                        && inf.getSuggestAuthority().startsWith("applications")) {
                    info = inf;
                }
            }
            mSearchView.setSearchableInfo(info);
        }

        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                Log.d("onQuery", "onClose");
                return false;
            }
        });
    }

    protected boolean isAlwaysExpanded() {
        return false;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.d("onQueryTextSubmit", query);
        /*if(!useGrid) {
            adapter.filter(query);
        } else {
            adapterGrid.filter(query);
        }*/

        itemTypeMoviesSelected = 7;
        typeMoviesPref.setNumberType(7);
        finish();
        startActivity(getIntent());

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.d("onQueryTextChange", newText);
        textSearch = newText + "";
        return false;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_free_movie) {
            // Handle the camera action
            if (typeMoviesPref.getNumberType() != 0) {
                itemTypeMoviesSelected = 0;
                typeMoviesPref.setNumberType(0);
            }
        } else if (id == R.id.nav_academic_films) {
            if (typeMoviesPref.getNumberType() != 1) {
                itemTypeMoviesSelected = 1;
                typeMoviesPref.setNumberType(1);
            }
        } else if (id == R.id.nav_ephemeral_film) {
            if (typeMoviesPref.getNumberType() != 2) {
                itemTypeMoviesSelected = 2;
                typeMoviesPref.setNumberType(2);
            }
        } else if (id == R.id.nav_television) {
            if (typeMoviesPref.getNumberType() != 3) {
                itemTypeMoviesSelected = 3;
                typeMoviesPref.setNumberType(3);
            }
        } else if (id == R.id.nav_horror_film) {
            if (typeMoviesPref.getNumberType() != 4) {
                itemTypeMoviesSelected = 4;
                typeMoviesPref.setNumberType(4);
            }
        } else if (id == R.id.nav_silent_films) {
            if (typeMoviesPref.getNumberType() != 5) {
                itemTypeMoviesSelected = 5;
                typeMoviesPref.setNumberType(5);
            }
        } else if (id == R.id.nav_home_movies) {
            if (typeMoviesPref.getNumberType() != 6) {
                itemTypeMoviesSelected = 6;
                typeMoviesPref.setNumberType(6);
            }
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_rate) {
            rateApp();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        finish();
        startActivity(getIntent());
        return true;
    }

    public void rateApp()
    {
        try
        {
            Intent rateIntent = rateIntentForUrl("market://details");
            startActivity(rateIntent);
        }
        catch (ActivityNotFoundException e)
        {
            Intent rateIntent = rateIntentForUrl("https://play.google.com/store/apps/details");
            startActivity(rateIntent);
        }
    }

    private Intent rateIntentForUrl(String url)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url, getPackageName())));
        int flags = Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK;
        if (Build.VERSION.SDK_INT >= 21) {
            flags |= Intent.FLAG_ACTIVITY_NEW_DOCUMENT;
        }
        else {
            //noinspection deprecation
            flags |= Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
        }
        intent.addFlags(flags);
        return intent;
    }
}
