package com.freemoviesapp.hi.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.freemoviesapp.hi.utils.VarsConstant;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.NativeExpressAdView;

import com.freemoviesapp.hi.R;
import com.freemoviesapp.hi.utils.CountFloatingPref;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;

import static com.freemoviesapp.hi.R.id.mIvClose;

public class VideoActivity extends AppCompatActivity implements View.OnClickListener {

    private String mUrl;
    private String mTitle;

    TextView tvTitle;
    JCVideoPlayerStandard jcVideoPlayerStandard;
    private boolean isFullScreen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        MobileAds.initialize(this, getBaseContext().getResources().getString(R.string.ad_unit_id));
        NativeExpressAdView adView = (NativeExpressAdView) findViewById(R.id.nativeAdsVideo);
        AdRequest request = new AdRequest.Builder()
                .addTestDevice("73EC02899E0DE80F79B9298CA568790D")
                .build();
        adView.loadAd(request);

        tvTitle = (TextView) findViewById(R.id.titleVideo);

        findViewById(mIvClose).setOnClickListener(this);
        findViewById(R.id.mImgSmall).setOnClickListener(this);
        findViewById(R.id.mImgFull).setOnClickListener(this);
        jcVideoPlayerStandard = (JCVideoPlayerStandard) findViewById(R.id.custom_videoplayer_standard);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            mUrl = b.getString("url");
            mTitle = b.getString("title");
        }

        if(jcVideoPlayerStandard != null) {
            jcVideoPlayerStandard.setUp(b.getString("url"), JCVideoPlayerStandard.SCREEN_LAYOUT_LIST, "");
            jcVideoPlayerStandard.startVideo();
        }

        tvTitle.setText(mTitle);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mIvClose:
                finish();
                break;

            case R.id.mImgSmall:
                CountFloatingPref pref = new CountFloatingPref(getBaseContext());
                int countCurrentFloatingRunning = pref.getNumberFloating();
                if(countCurrentFloatingRunning < VarsConstant.MAX_FLOATING_PLAYER) {
                    Intent floatingIntent = new Intent(VideoActivity.this, FloatingVideo.class);
                    floatingIntent.putExtra("url", mUrl);
                    floatingIntent.putExtra("title", mTitle);
                    startService(floatingIntent);

                    pref.setNumberFloating(countCurrentFloatingRunning + 1);
                }

                finish();
                break;
            case R.id.mImgFull:
//                jcVideoPlayerStandard.removeAllViews();
                jcVideoPlayerStandard.startWindowFullscreen();
                isFullScreen = true;
                break;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        if (JCVideoPlayer.backPress()) {
            return;
        }

        if(isFullScreen){
            jcVideoPlayerStandard.clearFullscreenLayout();
            isFullScreen = false;
            return;
        } else {
            if (!TextUtils.isEmpty(mUrl)) {
                CountFloatingPref pref = new CountFloatingPref(getBaseContext());
                int countCurrentFloatingRunning = pref.getNumberFloating();

                if(countCurrentFloatingRunning < VarsConstant.MAX_FLOATING_PLAYER) {
                    Intent floatingIntent = new Intent(VideoActivity.this, FloatingVideo.class);
                    floatingIntent.putExtra("url", mUrl);
                    floatingIntent.putExtra("title", mTitle);
                    startService(floatingIntent);

                    pref.setNumberFloating(countCurrentFloatingRunning + 1);
                }
            } else {
                super.onBackPressed();
            }
        }

        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        JCVideoPlayer.releaseAllVideos();
    }
}
