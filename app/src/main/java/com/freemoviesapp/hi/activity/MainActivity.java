package com.freemoviesapp.hi.activity;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.analytics.FirebaseAnalytics;

import com.freemoviesapp.hi.R;
import com.freemoviesapp.hi.adapter.MoviesAdapter;
import com.freemoviesapp.hi.adapter.MoviesGridAdapter;
import com.freemoviesapp.hi.adapter.TitleNavigationAdapter;
import com.freemoviesapp.hi.data.MoviesDBHelper;
import com.freemoviesapp.hi.data.MoviesDao;
import com.freemoviesapp.hi.data.MoviesFromJSONExtractor;
import com.freemoviesapp.hi.model.Movie;
import com.freemoviesapp.hi.model.SpinnerNavItem;
import com.freemoviesapp.hi.network.ApiClient;
import com.freemoviesapp.hi.utils.NetworkHelper;
import com.freemoviesapp.hi.utils.TypeMoviesPref;

import org.json.JSONException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements ApiClient.ResponseCallback, SearchView.OnQueryTextListener {
    private ConstraintLayout waitNotification;
    private ConstraintLayout waitForNewDataNotification;

    private RecyclerView moviesView;
    private MoviesAdapter adapter;
    private MoviesGridAdapter adapterGrid;
    private LinearLayoutManager layoutManager;
    private GridLayoutManager layoutGridManager;
    private boolean loading = true;
    private boolean useGrid = !true;

    private ApiClient client;
    private MoviesFromJSONExtractor moviesExtractor;
    private MoviesDao moviesDao;

    private FirebaseAnalytics firebaseAnalytics;

    // Title navigation Spinner data
    private ArrayList<SpinnerNavItem> navSpinner;

    // Navigation adapter
    private TitleNavigationAdapter adapterNavi;

    //
    private TypeMoviesPref typeMoviesPref;
    private int itemTypeMoviesSelected = 0;

    // search
    private SearchView mSearchView;
    private String textSearch = "";

    public void useGetData(int itemTypeMoviesSelected) {
        if (itemTypeMoviesSelected == 0) {
            client.getData(MainActivity.this);
        } else if (itemTypeMoviesSelected == 1) {
            client.getAcademicData(MainActivity.this);
        } else if (itemTypeMoviesSelected == 2) {
            client.getEphmeraData(MainActivity.this);
        } else if (itemTypeMoviesSelected == 3) {
            client.getTelevisionData(MainActivity.this);
        } else if (itemTypeMoviesSelected == 4) {
            client.getHorrorData(MainActivity.this);
        } else if (itemTypeMoviesSelected == 5) {
            client.getSilentData(MainActivity.this);
        } else if (itemTypeMoviesSelected == 6) {
            client.getHomeData(MainActivity.this);
        }
    }

    public void useGridLayout(boolean useGridLayout) {
        if (useGridLayout) {
            moviesView.setLayoutManager(layoutGridManager);
            moviesView.setHasFixedSize(true);
            moviesView.setAdapter(adapterGrid);
        } else {
            moviesView.setLayoutManager(layoutManager);
            moviesView.setAdapter(adapter);
        }
    }

    private int moviesToLoad = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

        setContentView(R.layout.activity_main);

        MobileAds.initialize(this, getBaseContext().getResources().getString(R.string.ad_unit_id));

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("73EC02899E0DE80F79B9298CA568790D").build();
        mAdView.loadAd(adRequest);

        // Action Bar
        Toolbar myToolbar = (Toolbar) findViewById(R.id.freemovieToolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getActionBar().setHomeButtonEnabled(true);
//        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.rgb(180, 15, 15)));
//        getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>Free Movies</font>"));

        // Enabling Spinner dropdown navigation
//        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

        // Spinner title navigation data
        navSpinner = new ArrayList<SpinnerNavItem>();
        navSpinner.add(new SpinnerNavItem("Free Movies"));
        navSpinner.add(new SpinnerNavItem("Academic Films"));
        navSpinner.add(new SpinnerNavItem("Ephemeral Films"));
        navSpinner.add(new SpinnerNavItem("Television"));
        navSpinner.add(new SpinnerNavItem("Horror Films"));
        navSpinner.add(new SpinnerNavItem("Silent Films"));
        navSpinner.add(new SpinnerNavItem("Home Movies"));

        // title drop down adapter
        adapterNavi = new TitleNavigationAdapter(getApplicationContext(), navSpinner);

        typeMoviesPref = new TypeMoviesPref(getBaseContext());
        itemTypeMoviesSelected = typeMoviesPref.getNumberType();
        // assigning the spinner navigation
        /*android.support.v7.app.ActionBar.OnNavigationListener onNavigationListener = new android.support.v7.app.ActionBar.OnNavigationListener() {
            @Override
            public boolean onNavigationItemSelected(int itemPosition, long itemId) {
                if (typeMoviesPref.getNumberType() != itemPosition) {
                    itemTypeMoviesSelected = itemPosition;
                    typeMoviesPref.setNumberType(itemPosition);
                    finish();
                    startActivity(getIntent());
                }

                return false;
            }
        };
        getSupportActionBar().setListNavigationCallbacks(adapterNavi, onNavigationListener);
        getSupportActionBar().setSelectedNavigationItem(itemTypeMoviesSelected);*/

        Spinner spinnerType = (Spinner) findViewById(R.id.spinnerType);
        spinnerType.setAdapter(adapterNavi);
        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (typeMoviesPref.getNumberType() != position) {
                    itemTypeMoviesSelected = position;
                    typeMoviesPref.setNumberType(position);
                    finish();
                    startActivity(getIntent());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerType.setSelection(itemTypeMoviesSelected);

        if(!NetworkHelper.isNetworkAvailable(getBaseContext())) {
            findViewById(R.id.layoutNoInternet).setVisibility(View.VISIBLE);
            findViewById(R.id.activity_main).setVisibility(View.GONE);
        } else {
            findViewById(R.id.layoutNoInternet).setVisibility(View.GONE);
            findViewById(R.id.activity_main).setVisibility(View.VISIBLE);
        }

        findViewById(R.id.bntRefresh).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(getIntent());
            }
        });

        client = new ApiClient();
//        client.stopAsynctaskRunning();
        moviesExtractor = new MoviesFromJSONExtractor();
        moviesDao = new MoviesDBHelper(this, MoviesDBHelper.VERSION);

        waitNotification = (ConstraintLayout) findViewById(R.id.retrievingDataNotification);
        waitForNewDataNotification = (ConstraintLayout) findViewById(R.id.fetchingNewData);

        moviesView = (RecyclerView) findViewById(R.id.moviesList);
        moviesView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (moviesToLoad == 0 && dy > 0) {
                    if (!useGrid) {
                        int visibleItemCount = layoutManager.getChildCount();
                        int totalItemCount = layoutManager.getItemCount();
                        int pastVisibleItems = layoutManager.findFirstVisibleItemPosition();
                        if (loading) {
                            if (visibleItemCount + pastVisibleItems >= totalItemCount) {
                                loading = false;
                                waitForNewDataNotification.setVisibility(View.VISIBLE);
//                                client.getData(MainActivity.this);
                                useGetData(itemTypeMoviesSelected);
                            }
                        }
                    } else {
                        int visibleItemCount = layoutGridManager.getChildCount();
                        int totalItemCount = layoutGridManager.getItemCount();
                        int pastVisibleItems = layoutGridManager.findFirstVisibleItemPosition();
                        if (loading) {
                            if (visibleItemCount + pastVisibleItems >= totalItemCount) {
                                loading = false;
                                waitForNewDataNotification.setVisibility(View.VISIBLE);
//                                client.getData(MainActivity.this);
                                useGetData(itemTypeMoviesSelected);
                            }
                        }
                    }
                }
            }
        });

        layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutGridManager = new GridLayoutManager(MainActivity.this, 2);
        layoutGridManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position % 7 == 0) {
                    return 2;
                }

                return 1;
            }
        });
        moviesView.setLayoutManager(layoutManager);
        moviesView.setItemAnimator(new DefaultItemAnimator());
        adapter = new MoviesAdapter(this);
        adapterGrid = new MoviesGridAdapter(this);
        moviesView.setAdapter(adapter);

//        client.getData(this);
        useGetData(itemTypeMoviesSelected);

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        firebaseAnalytics.setMinimumSessionDuration(5000);
        firebaseAnalytics.setSessionTimeoutDuration(1000000);

//        permissionHelper = new PermissionHelper(this);
//        permissionHelper.checkAllPermissions();
    }

    @Override
    public void onSuccess(String responseBody) {
        waitNotification.setVisibility(View.GONE);
        waitForNewDataNotification.setVisibility(View.VISIBLE);
        loading = true;
        moviesView.setVisibility(View.VISIBLE);

        try {
            Log.d("kaka Success", responseBody);
            extractMoviesFromResponse(responseBody);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Exception e) {
        waitNotification.setVisibility(View.GONE);
        waitForNewDataNotification.setVisibility(View.GONE);
        loading = true;
        e.printStackTrace();
    }

    private void extractMoviesFromResponse(String responseBody) throws JSONException {
        ArrayList<Movie> movies;
        movies = moviesExtractor.parse(responseBody);
        moviesToLoad = movies.size();

        for (Movie movie : movies) {
            Movie savedMovie = moviesDao.findByIdentifier(movie.getIdentifier(), itemTypeMoviesSelected);
            if (savedMovie == null) {
                fetchMovieUrls(movie);
            } else {
                adapter.add(savedMovie);
                adapter.notifyDataSetChanged();
                adapterGrid.add(savedMovie);
                adapterGrid.notifyDataSetChanged();
                if (--moviesToLoad == 0) {
                    waitForNewDataNotification.setVisibility(View.GONE);
                }
            }
        }
    }

    private void fetchMovieUrls(final Movie movie) {
        client.getFilesData(movie.getIdentifier(), new ApiClient.ResponseCallback() {
            @Override
            public void onSuccess(String responseBody) {
                try {
                    getUrlsFromXml(responseBody);
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
            }

            private void getUrlsFromXml(String responseBody) throws XmlPullParserException, IOException {
                ArrayList<String> fileNames = parseFiles(responseBody);
                getThumbnailUrl(fileNames);
                getVideoUrl(fileNames);
//                getDes(ApiClient.ARCHIVE_DETAILS + movie.getIdentifier());
                adapter.add(movie);
                adapter.notifyDataSetChanged();
                adapterGrid.add(movie);
                adapterGrid.notifyDataSetChanged();
                moviesDao.add(movie, itemTypeMoviesSelected);
                if (--moviesToLoad == 0) {
                    waitForNewDataNotification.setVisibility(View.GONE);
                }
            }

            private void getVideoUrl(ArrayList<String> fileNames) {
                for (String filename : fileNames) {
                    String extension = filename.substring(filename.lastIndexOf('.') + 1, filename.length());
                    if (extension.equals("mp4")) {
                        movie.setVideoUrl(filename);
                        // [START screen_view_hit]
//                        Log.i("MainActivity", "Video FILE: " + filename);
//                        mTracker.setScreenName("Video FILE" + filename);
//                        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
                        // [END screen_view_hit]
                        break;
                    }
                }
            }

            private void getThumbnailUrl(ArrayList<String> fileNames) {
                for (String filename : fileNames) {
                    String extension = filename.substring(filename.lastIndexOf('.') + 1, filename.length());
                    if (extension.equals("jpg") || extension.equals("png")) {
                        // [START screen_view_hit]
//                        Log.i("MainActivity", "Image FILE: " + filename);
//                        mTracker.setScreenName("Image FILE" + filename);
//                        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
                        // [END screen_view_hit]
                        movie.setThumbnailUrl(filename);
                        break;
                    }
                }
            }

            private void getDes(String url) {
                // Connect to the web site
                /*Document document = null;

                if (android.os.Build.VERSION.SDK_INT > 9) {
                    StrictMode.ThreadPolicy policy =
                            new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                }
                try {
                    document = Jsoup.connect(url).get();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // Using Elements to get the Meta data
                if(document == null) {
                    movie.setVideoDes("");
                } else {

                    Elements description = document.select("meta[name=description]");
                    //Locate the content attribute
                    movie.setVideoDes(description.attr("content"));
                }*/

//                movie.setVideoDes("");
            }
        });
    }

    private ArrayList<String> parseFiles(String xml) throws XmlPullParserException, IOException {
        ArrayList<String> filenames = new ArrayList<>();

        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser parser = factory.newPullParser();
        parser.setInput(new StringReader(xml));
        int eventType = parser.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                if (parser != null && "file".equals(parser.getName())) {
                    filenames.add(parser.getAttributeValue(0));
                }
            }
            try {
                eventType = parser.next();
            } catch (Exception ignored) {
            }
        }
        return filenames;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_setting, menu);

        // Associate searchable configuration with the SearchView
        /*SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchView = (SearchView) menu.findItem(R.id.action_Search)
                .getActionView();
        mSearchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));*/

        MenuItem searchItem = menu.findItem(R.id.action_Search);
        mSearchView = (SearchView) searchItem.getActionView();
        setupSearchView(searchItem);

        ImageView closeButton = (ImageView)mSearchView.findViewById(R.id.search_close_btn);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!useGrid) {
                    adapter.filter("");
                } else {
                    adapterGrid.filter("");
                }
            }
        });

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                Log.d("onQuery", "onMenuItemActionCollapse");
                if(!useGrid) {
                    adapter.filter("");
                } else {
                    adapterGrid.filter("");
                }
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                Log.d("onQuery", "onMenuItemActionExpand");
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_GirdView:
                // change to gridview
                useGridLayout(true);
                useGrid = true;
                return true;
            case R.id.action_ListView:
                // change to listview
                useGridLayout(false);
                useGrid = false;
                return true;
            case R.id.action_SORTBY:
                // sort by
                useGridLayout(useGrid);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        minimizeApp();
    }

    public void minimizeApp() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10) {
            permissionHelper.checkAllPermissions();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 10) {
            permissionHelper.checkAllPermissions();
        }
    }*/

    private void setupSearchView(MenuItem searchItem) {

        if (isAlwaysExpanded()) {
            mSearchView.setIconifiedByDefault(false);
        } else {
            searchItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM
                    | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        }

        if(mSearchView == null) {
            mSearchView = (SearchView) searchItem.getActionView();
        }
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if (searchManager != null) {
            List<SearchableInfo> searchables = searchManager.getSearchablesInGlobalSearch();

            SearchableInfo info = searchManager.getSearchableInfo(getComponentName());
            for (SearchableInfo inf : searchables) {
                if (inf.getSuggestAuthority() != null
                        && inf.getSuggestAuthority().startsWith("applications")) {
                    info = inf;
                }
            }
            mSearchView.setSearchableInfo(info);
        }

        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                Log.d("onQuery", "onClose");
                return false;
            }
        });
    }

    protected boolean isAlwaysExpanded() {
        return false;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        Log.d("onQueryTextSubmit", query);
        if(!useGrid) {
            adapter.filter(query);
        } else {
            adapterGrid.filter(query);
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.d("onQueryTextChange", newText);
        textSearch = newText + "";
        return false;
    }
}
