package com.freemoviesapp.hi.activity;

import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.VideoView;

import com.freemoviesapp.hi.utils.VarsConstant;
import com.malmstein.fenster.controller.SimpleMediaFensterPlayerController;
import com.freemoviesapp.hi.R;
import com.freemoviesapp.hi.utils.CountFloatingPref;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;

public class FloatingVideo extends Service {
    private static final String TAG = "FloatingService";
    private WindowManager mWindowManager;
    private View mView;
    private String mUrl;
    private String mTitle;
    private VideoView mVideoView;
    private SimpleMediaFensterPlayerController mMediaController;

    CountFloatingPref pref = null;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //TODO do something useful
        mUrl = intent.getStringExtra("url");
        mTitle = intent.getStringExtra("title");
        mUrl = mUrl.replaceAll("https", "http");
        mVideoView.setVideoPath(mUrl);
        mVideoView.start();

        return Service.START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.floating_video, null);
        final WindowManager.LayoutParams parameters = new WindowManager.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_TOAST,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        parameters.gravity = Gravity.CENTER | Gravity.CENTER;
        parameters.x = 0;
        parameters.y = 0;
        mWindowManager.addView(mView, parameters);
        mView.setOnTouchListener(new View.OnTouchListener() {
            WindowManager.LayoutParams updatedParameters = parameters;
            double x;
            double y;
            double pressedX;
            double pressedY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        x = updatedParameters.x;
                        y = updatedParameters.y;
                        pressedX = event.getRawX();
                        pressedY = event.getRawY();
                        break;

                    case MotionEvent.ACTION_MOVE:
                        updatedParameters.x = (int) (x + (event.getRawX() - pressedX));
                        updatedParameters.y = (int) (y + (event.getRawY() - pressedY));
                        mWindowManager.updateViewLayout(mView, updatedParameters);
                    default:
                        break;
                }
                return false;
            }
        });

        mView.findViewById(R.id.mIvClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JCVideoPlayer.releaseAllVideos();
                mWindowManager.removeView(mView);
                stopSelf();
                VarsConstant.hasFloatingPlayer = false;
                pref = new CountFloatingPref(getBaseContext());
                int countCurrentFloatingRunning = pref.getNumberFloating();
                if(countCurrentFloatingRunning > 0) {
                    pref.setNumberFloating(countCurrentFloatingRunning - 1);
                }
            }
        });

        mMediaController = (SimpleMediaFensterPlayerController) mView.findViewById(R.id.mMediaController);
        mVideoView = (VideoView) mView.findViewById(R.id.mVideoView);
        final CheckBox cbPlay = (CheckBox) mView.findViewById(R.id.mCbPlay);
        cbPlay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (mVideoView.isPlaying()) {
                        mVideoView.pause();
                    } else {
                        mVideoView.start();
                    }
                }
            }
        });
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mVideoView.start();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mMediaController.setVisibility(View.GONE);
                        cbPlay.setVisibility(View.VISIBLE);
                    }
                }, 7000);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(pref == null) {
            pref = new CountFloatingPref(getBaseContext());
        }
        int countCurrentFloatingRunning = pref.getNumberFloating();
        if(countCurrentFloatingRunning > 0) {
            pref.setNumberFloating(countCurrentFloatingRunning - 1);
        }
        if(VarsConstant.hasFloatingPlayer) {
            JCVideoPlayer.releaseAllVideos();
            mWindowManager.removeView(mView);
            stopSelf();
        }
        Log.d("kaka", "destroy");
    }
}