package com.freemoviesapp.hi.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.cuong.cdroid.util.PermissionUtils;
import com.freemoviesapp.hi.R;
import com.freemoviesapp.hi.utils.NetworkHelper;

import java.util.ArrayList;
import java.util.List;

public class SplashActivity extends AppCompatActivity {

    private View layoutPermission;
    private List<PermissionCheckbox> checkboxes;

    private final String[] permissions = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.SYSTEM_ALERT_WINDOW
    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slash_main);

        layoutPermission = findViewById(R.id.layoutRequestPermission);

        if(!NetworkHelper.isNetworkAvailable(getBaseContext())) {
            Toast.makeText(getBaseContext(), "There is no Internet connection", Toast.LENGTH_LONG).show();
        }

        Log.d("kaka", "5");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkboxes = new ArrayList<>();
            checkboxes.add(new PermissionCheckbox(R.id.cbPermissionWriteExternalStorage, permissions[0]));
            checkboxes.add(new PermissionCheckbox(R.id.cbPermissionReadExternalStorage, permissions[1]));
            checkboxes.add(new PermissionCheckbox(R.id.cbPermissionReadPhoneState, permissions[2]));
            checkboxes.add(new PermissionCheckbox(R.id.cbPermissionManageOverlay, permissions[3]));
            layoutPermission.setVisibility(View.INVISIBLE);

            if (needRequestPermission()){
                layoutPermission.setVisibility(View.VISIBLE);
            }
        } else {
            layoutPermission.setVisibility(View.GONE);
            openApp();
        }
    }

    private boolean needRequestPermission() {
        for (PermissionCheckbox cb : checkboxes) {
            cb.refresh();
            if (!cb.checkSelfPermission()) {
                cb.requestPermission();
                return true;
            }
        }

        openApp();
        return false;
    }

    private void openApp(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(getApplicationContext(), Main2Activity.class);
                startActivity(i);
                finish();
            }
        }, 1000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1104) {
            needRequestPermission();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1234) {
            needRequestPermission();
        }
    }


    public class PermissionCheckbox {
        public CheckBox cbPermission;
        public String permission;

        public PermissionCheckbox(int cbId, String per) {
            cbPermission = (CheckBox) findViewById(cbId);
            permission = per;
            cbPermission.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (!checkSelfPermission()) {
                        requestPermission();
                    }
                }
            });

            refresh();
        }

        public void requestPermission() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permission.equals(permissions[3])) {
                Intent i = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(i, 1234);
            } else {
                PermissionUtils.requestPermission(SplashActivity.this, permission, 1104);
            }
        }

        public boolean checkSelfPermission() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permission.equals(permissions[3])) {
                return Settings.canDrawOverlays(SplashActivity.this);
            }
            return ContextCompat.checkSelfPermission(SplashActivity.this, permission) == PackageManager.PERMISSION_GRANTED;
        }

        public void refresh() {
            if (!checkSelfPermission()) {
                cbPermission.setChecked(false);
                cbPermission.setAlpha(0.5f);
            } else {
                cbPermission.setChecked(true);
                cbPermission.setAlpha(1.0f);
            }
        }
    }
}


