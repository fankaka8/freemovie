package com.freemoviesapp.hi.data;

import android.support.annotation.NonNull;
import android.util.Log;

import com.freemoviesapp.hi.model.Movie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MoviesFromJSONExtractor {
    public static final String RESPONSE = "response";
    public static final String NUM_FOUND = "numFound";
    public static final String DOCS = "docs";
    public static final String DESCRIPTION = "description";
    public static final String TITLE = "title";
    public static final String IDENTIFIER = "identifier";

    private int total = 0;

    public int getTotal() {
        return total;
    }

    public ArrayList<Movie> parse(String json) throws JSONException {
        Log.d("json", json);
        json = handleCallbackWrap(json);
        JSONObject object = new JSONObject(json);
        JSONObject response = object.getJSONObject(RESPONSE);
        setTotal(response);
        ArrayList<Movie> movies = extractMovies(response);
        return movies;
    }

    @NonNull
    private ArrayList<Movie> extractMovies(JSONObject response) throws JSONException {
        JSONArray docs = response.getJSONArray(DOCS);
        ArrayList<Movie> movies = new ArrayList<>();
        for (int i = 0; i < docs.length(); ++i) {
            JSONObject movieObj = docs.getJSONObject(i);
            movies.add(new Movie(
                    getNullableString(movieObj, TITLE),
                    getNullableString(movieObj, IDENTIFIER)));
        }
        return movies;
    }

    private void setTotal(JSONObject response) throws JSONException {
        if (total == 0) {
            total = response.getInt(NUM_FOUND);
        }
    }

    /**
     * Json from archive.org comes in form 'callback(*JSON*)'
     * This method gets rid of this callback wrap
     */
    @NonNull
    private String handleCallbackWrap(String json) throws JSONException {
        if (json.length() < 11) {
            throw new JSONException("Empty JSON");
        }
        json = json.substring(9, json.length() - 1);
        return json;
    }

    private String getNullableString(JSONObject obj, String field) throws JSONException {
        if (obj.isNull(field)) {
            return "";
        } else {
            return obj.getString(field);
        }
    }
}
