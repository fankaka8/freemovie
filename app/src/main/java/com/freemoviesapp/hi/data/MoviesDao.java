package com.freemoviesapp.hi.data;

import com.freemoviesapp.hi.model.Movie;

public interface MoviesDao {
    void add(Movie movie, int type);
    Movie findByIdentifier(String identifier, int type);
}
