package com.freemoviesapp.hi.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.freemoviesapp.hi.model.Movie;

public class MoviesDBHelper extends SQLiteOpenHelper implements MoviesDao {
    public static final int VERSION = 5;
    private static final String NAME = "movies.db";

    public MoviesDBHelper(Context context, int version) {
        super(context, NAME, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE movies (" +
                "identifier TEXT PRIMARY KEY," +
                "title TEXT," +
                "thumbnail TEXT," +
                "video TEXT)");

        db.execSQL("CREATE TABLE cuturals (" +
                "identifier TEXT PRIMARY KEY," +
                "title TEXT," +
                "thumbnail TEXT," +
                "video TEXT)");

        db.execSQL("CREATE TABLE ephmeras (" +
                "identifier TEXT PRIMARY KEY," +
                "title TEXT," +
                "thumbnail TEXT," +
                "video TEXT)");

        db.execSQL("CREATE TABLE televisions (" +
                "identifier TEXT PRIMARY KEY," +
                "title TEXT," +
                "thumbnail TEXT," +
                "video TEXT)");

        db.execSQL("CREATE TABLE horrors (" +
                "identifier TEXT PRIMARY KEY," +
                "title TEXT," +
                "thumbnail TEXT," +
                "video TEXT)");

        db.execSQL("CREATE TABLE silents (" +
                "identifier TEXT PRIMARY KEY," +
                "title TEXT," +
                "thumbnail TEXT," +
                "video TEXT)");

        db.execSQL("CREATE TABLE homes (" +
                "identifier TEXT PRIMARY KEY," +
                "title TEXT," +
                "thumbnail TEXT," +
                "video TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS movies");
        db.execSQL("DROP TABLE IF EXISTS cuturals");
        db.execSQL("DROP TABLE IF EXISTS ephmeras");
        db.execSQL("DROP TABLE IF EXISTS televisions");
        db.execSQL("DROP TABLE IF EXISTS horrors");
        db.execSQL("DROP TABLE IF EXISTS silents");
        db.execSQL("DROP TABLE IF EXISTS homes");
        onCreate(db);
    }

    @Override
    public void add(Movie movie, int type) {
        SQLiteDatabase db = getWritableDatabase();
        SQLiteStatement statement = null;
        if(type == 0) {
            statement = db.compileStatement(
                    "INSERT INTO movies(identifier, title, thumbnail, video) VALUES(?, ?, ?, ?)"
            );
        } else if (type == 1) {
            statement = db.compileStatement(
                    "INSERT INTO cuturals(identifier, title, thumbnail, video) VALUES(?, ?, ?, ?)"
            );
        } else if (type == 2) {
            statement = db.compileStatement(
                    "INSERT INTO ephmeras(identifier, title, thumbnail, video) VALUES(?, ?, ?, ?)"
            );
        } else if (type == 3) {
            statement = db.compileStatement(
                    "INSERT INTO televisions(identifier, title, thumbnail, video) VALUES(?, ?, ?, ?)"
            );
        } else if (type == 4) {
            statement = db.compileStatement(
                    "INSERT INTO horrors(identifier, title, thumbnail, video) VALUES(?, ?, ?, ?)"
            );
        } else if (type == 5) {
            statement = db.compileStatement(
                    "INSERT INTO silents(identifier, title, thumbnail, video) VALUES(?, ?, ?, ?)"
            );
        } else if (type == 6) {
            statement = db.compileStatement(
                    "INSERT INTO homes(identifier, title, thumbnail, video) VALUES(?, ?, ?, ?)"
            );
        }
        statement.bindString(1, movie.getIdentifier());
        statement.bindString(2, movie.getTitle());
        statement.bindString(3, movie.getThumbnailUrl());
        statement.bindString(4, movie.getVideoUrl());
        statement.execute();
    }

    @Override
    public Movie findByIdentifier(String identifier, int type) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = null;
        if(type == 0) {
            cursor = db.rawQuery("SELECT * FROM movies WHERE identifier = ?", new String[]{identifier});
        } else if(type == 1) {
            cursor = db.rawQuery("SELECT * FROM cuturals WHERE identifier = ?", new String[]{identifier});
        } else if(type == 2) {
            cursor = db.rawQuery("SELECT * FROM ephmeras WHERE identifier = ?", new String[]{identifier});
        } else if(type == 3) {
            cursor = db.rawQuery("SELECT * FROM televisions WHERE identifier = ?", new String[]{identifier});
        } else if(type == 4) {
            cursor = db.rawQuery("SELECT * FROM horrors WHERE identifier = ?", new String[]{identifier});
        } else if(type == 5) {
            cursor = db.rawQuery("SELECT * FROM silents WHERE identifier = ?", new String[]{identifier});
        } else if(type == 6) {
            cursor = db.rawQuery("SELECT * FROM homes WHERE identifier = ?", new String[]{identifier});
        } else {
            cursor = db.rawQuery("SELECT * FROM movies WHERE identifier = ?", new String[]{identifier});
        }
        if (cursor == null || !cursor.moveToFirst()) {
            return null;
        }
        return new Movie(
                cursor.getString(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3)
        );
    }
}
