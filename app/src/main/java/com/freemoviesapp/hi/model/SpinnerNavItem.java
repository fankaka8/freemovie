package com.freemoviesapp.hi.model;

/**
 * Created by trunglee on 2/12/17.
 */

public class SpinnerNavItem {

    private String title;

    public SpinnerNavItem(String title) {
        this.title = title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
