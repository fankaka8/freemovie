package com.freemoviesapp.hi.model;

import com.freemoviesapp.hi.network.ApiClient;

public class Movie {
    public static final int TYPE_CONTENT = 0;
    public static final int TYPE_AD = 1;

    private String title = "";
    private String identifier = "";

    private String thumbnailUrl = "";
    private String videoUrl = "";

    private int contentType;

    public int getContentType() {
        return contentType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String fileName) {
        thumbnailUrl = buildFileUrl(fileName);
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String fileName) {
        videoUrl = buildFileUrl(fileName);
    }

    public Movie(String identifier, String title, String thumbnailUrl, String videoUrl) {
        this.identifier = identifier;
        this.title = title;
        this.thumbnailUrl = thumbnailUrl;
        this.videoUrl = videoUrl;
    }

    public Movie(String title, String identifier) {
        this.title = title;
        this.identifier = identifier;
        this.contentType = TYPE_CONTENT;
    }

    public Movie() {
        this.contentType = TYPE_AD;
    }

    private String buildFileUrl(String fileName) {
        return ApiClient.ARCHIVE_DOWNLOAD + identifier + "/" + fileName;
    }
}
