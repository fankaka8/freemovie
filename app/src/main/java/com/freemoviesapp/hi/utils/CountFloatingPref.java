package com.freemoviesapp.hi.utils;

import android.content.Context;

/**
 * Created by trunglee on 1/3/17.
 */

public class CountFloatingPref extends PrefHelper {

    public CountFloatingPref(Context context) {
        super(context);
    }

    @Override
    String getPrefName() {
        return "count_floating";
    }

    public int getNumberFloating() {
        return pref.getInt("floating_number", 0);
    }

    public void setNumberFloating(int numberFloating) {
        commit("floating_number", numberFloating);
    }
}
