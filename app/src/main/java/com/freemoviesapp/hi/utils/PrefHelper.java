package com.freemoviesapp.hi.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Clover on 12/9/2016.
 * Contact: vcuong11s@gmail.com or unme.rf@gmail.com
 */

public abstract class PrefHelper {
    protected SharedPreferences pref;
    private SharedPreferences.Editor editor;

    public PrefHelper(Context context) {
        pref = context.getSharedPreferences(getPrefName(), 0);
    }

    abstract String getPrefName();

    protected void commit(String key, String value) {
        editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    protected void commit(String key, int value) {
        editor = pref.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    protected void commit(String key, boolean value) {
        editor = pref.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    protected void commit(String key, long value) {
        editor = pref.edit();
        editor.putLong(key, value);
        editor.commit();
    }
}
