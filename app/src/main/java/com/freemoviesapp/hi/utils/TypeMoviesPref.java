package com.freemoviesapp.hi.utils;

import android.content.Context;

/**
 * Created by trunglee on 1/3/17.
 */

public class TypeMoviesPref extends PrefHelper {

    public TypeMoviesPref(Context context) {
        super(context);
    }

    @Override
    String getPrefName() {
        return "type_movies_pref";
    }

    public int getNumberType() {
        return pref.getInt("type_movies", 0);
    }

    public void setNumberType(int numberType) {
        commit("type_movies", numberType);
    }

    public int getVersionDataForType() {
        return pref.getInt("version_for_type", 0);
    }

    public void setVersionDataForType(int versionDataForType) {
        commit("version_for_type", versionDataForType);
    }
}
