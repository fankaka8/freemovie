package com.freemoviesapp.hi.utils;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.cuong.cdroid.util.PermissionUtils;

/**
 * Created by Clover on 12/13/2016.
 * Contact: vcuong11s@gmail.com or unme.rf@gmail.com
 */

public class PermissionHelper {
    private AppCompatActivity context;

    public PermissionHelper(AppCompatActivity context)  {
        this.context = context;
    }

    private String[] permissions = new String[]{
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
//            android.Manifest.permission.DISABLE_KEYGUARD,
            android.Manifest.permission.ACCESS_NETWORK_STATE,
//            android.Manifest.permission.RECEIVE_BOOT_COMPLETED,
            android.Manifest.permission.READ_PHONE_STATE,
            android.Manifest.permission.WAKE_LOCK
    };

    public void checkAllPermissions() {
        for (String permission : permissions) {
            if (!checkPermission(permission)) {
                return;
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(context)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + context.getPackageName()));
                context.startActivityForResult(intent, 10);
            }
        }
    }

    private boolean checkPermission(String permission) {
        int permissionCheck = ContextCompat.checkSelfPermission(context, permission);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            PermissionUtils.requestPermissions(context, new String[]{permission}, 10);
            return false;
        }
        return true;
    }
}
