package com.freemoviesapp.hi.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cuong.cdroid.util.DataUtils;
import com.freemoviesapp.hi.activity.FloatingVideo;
import com.freemoviesapp.hi.utils.CountFloatingPref;
import com.freemoviesapp.hi.utils.VarsConstant;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.android.gms.ads.AdRequest;
import com.freemoviesapp.hi.R;
import com.freemoviesapp.hi.activity.VideoActivity;
import com.freemoviesapp.hi.model.Movie;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

public class MoviesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String VIDEO_SOURCE_TOKEN = "videoSource";
    private static final String AUTO_PLAY_TOKEN = "autoPlay";
    private static final String FULL_SCREEN_TOKEN = "setFullScreen";

    private ArrayList<Movie> movies;
    private ArrayList<Movie> movieslist = null;
    private Context context;
    private InterstitialAd interstitialAd;
    private String videoUrl = null;
    private String videoTitle = null;
    private String videoUrlThumbnail = null;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private ImageView thumbnail;
        private ConstraintLayout movieItem;

        public void setTitleText(String text) {
            title.setText(text);
        }

        public void downloadThumbnail(String url) {
            if (!DataUtils.isNull(url)) {
                Picasso.with(context).load(url).placeholder(R.drawable.placeholder).into(thumbnail);
            }
        }

        public void setOnPlay(final String url, final String title, final String urlThumbnail) {
            movieItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CountFloatingPref pref = new CountFloatingPref(context);
                    int countCurrentFloatingRunning = pref.getNumberFloating();

                    if(countCurrentFloatingRunning >= VarsConstant.MAX_FLOATING_PLAYER) {
                        VarsConstant.hasFloatingPlayer = true;
                        context.stopService(new Intent(context, FloatingVideo.class));
                    }

                    videoUrl = url;
                    videoTitle = title;
                    videoUrlThumbnail = urlThumbnail;
                    if (interstitialAd.isLoaded()) {
                        interstitialAd.show();
                    } else {
                        playVideo();
                    }
                }
            });
        }

        public ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.movieTitle);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            movieItem = (ConstraintLayout) view.findViewById(R.id.movieItem);
        }
    }

    public class AdViewHolder extends RecyclerView.ViewHolder {
        public NativeExpressAdView nativeAds;

        public AdViewHolder(View view) {
            super(view);
            nativeAds = (NativeExpressAdView) view.findViewById(R.id.nativeAds);
            AdRequest adRequest = new AdRequest.Builder().addTestDevice("73EC02899E0DE80F79B9298CA568790D").build();
            nativeAds.loadAd(adRequest);
        }
    }

    public MoviesAdapter(Context context) {
        movies = new ArrayList<>();
        movieslist = new ArrayList<>();
        this.context = context;
        interstitialAd = new InterstitialAd(context);
        interstitialAd.setAdUnitId(context.getResources().getString(R.string.interstitial_ad_id));
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
                playVideo();
            }
        });
        requestNewInterstitial();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case Movie.TYPE_CONTENT: {
                View view = inflater.inflate(R.layout.movies_item, parent, false);
                holder = new ViewHolder(view);
            }
            break;
            case Movie.TYPE_AD: {
                View view = null;
                view = inflater.inflate(R.layout.ad_banner, parent, false);
                holder = new AdViewHolder(view);
            }
            break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Movie movie = movies.get(position);
        switch (holder.getItemViewType()) {
            case Movie.TYPE_CONTENT: {
                ((ViewHolder) holder).setTitleText(movie.getTitle());
                ((ViewHolder) holder).setOnPlay(movie.getVideoUrl(), movie.getTitle(), movie.getThumbnailUrl());
                ((ViewHolder) holder).downloadThumbnail(movie.getThumbnailUrl());
            }
            break;
            case Movie.TYPE_AD: {

            }
            break;
        }
    }

    @Override
    public int getItemViewType(int position) {
//        if(position == 0 || position % 6 == 0) {
//            return Movie.TYPE_AD;
//        }
        return movies.get(position).getContentType();
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public void add(Movie movie) {
        if (movies.size() % 6 == 0) {
            movies.add(new Movie());
            movieslist.add(new Movie());
        } else {
            movies.add(movie);
            movieslist.add(movie);
        }
        notifyDataSetChanged();
    }

    private void requestNewInterstitial() {
        AdRequest request = new AdRequest.Builder().addTestDevice("73EC02899E0DE80F79B9298CA568790D")
                .build();
        interstitialAd.loadAd(request);
    }

    private void playVideo() {
        Intent intent = new Intent(MoviesAdapter.this.context, VideoActivity.class);
        Log.d("VIDEO URL", videoUrl+"");

        intent.putExtra("url", videoUrl);
        intent.putExtra("title", videoTitle);
        intent.putExtra("thumbnail", videoUrlThumbnail);

        context.startActivity(intent);
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());

        if(movies != null) {
            movies.clear();
        } else {
            movies = new ArrayList<>();
        }

        Log.d("kaka", charText);
        if (charText.length() == 0) {
            movies = movieslist;
        } else {
            for (Movie movie : movieslist) {
                if (movie.getTitle()/*.toLowerCase(Locale.getDefault())*/
                        .contains(charText)) {
                    movies.add(movie);
                }
            }
        }
        notifyDataSetChanged();
    }
}