package com.freemoviesapp.hi.network;

import android.os.AsyncTask;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import java.io.IOException;

public class ApiClient {

    public static final String ARCHIVE_DOWNLOAD = "https://archive.org/download/";
    public static final String ARCHIVE_DETAILS = "https://archive.org/details/";
    public static final String FREE_MOVIES = "moviesandfilms";
    public static final String ACADEMIC_FILMS = "culturalandacademicfilms";
    public static final String EPHEMERA = "ephemera";
    public static final String TELEVISION = "television";
    public static final String HORROR_FILMS = "SciFi_Horror";
    public static final String SILENT_FILMS = "silent_films";
    public static final String HOME_MOVIES = "home_movies";

    private  DownloadSearchTask downloadSearchTask;
    private  DownloadTask downloadTask;
    private  DownloadAcademicTask downloadAcademicTask;
    private  DownloadEphmeraTask downloadEphmeraTask;
    private  DownloadTelevisionTask downloadTelevisionTask;
    private  DownloadHorrorTask downloadHhorrorTask;
    private  DownloadSilentTask downloadSilentTask;
    private  DownloadHomeTask downloadHomeTask;

    public interface ResponseCallback {
        void onSuccess(String responseBody);
        void onFailure(Exception e);
    }

    private class DownloadSearchTask extends AsyncTask<ResponseCallback, Void, String> {

        private OkHttpClient client;
        private Request request;
        private ResponseCallback callback;
        private String searchText;

        public DownloadSearchTask(String searchText) {
            this.searchText = searchText;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            client = new OkHttpClient();
            request = new Request.Builder()
                    .url(buildQuery(searchText))
                    .build();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            if (response.isEmpty()) {
                callback.onFailure(new Exception("Response is empty"));
            } else {
                callback.onSuccess(response);
            }
        }

        @Override
        protected String doInBackground(ResponseCallback... params) {
            callback = params[0];
            try {
                return client.newCall(request).execute().body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }
        }

        protected String buildQuery(String search) {
            String buildQuery = "";
            buildQuery = "https://archive.org/advancedsearch.php?" +
                    "q=" + search +
                    "&fl%5B%5D=title&sort%5B%5D=&sort%5B%5D=&sort%5B%5D=&" +
                    "rows=" + String.valueOf(rows) + "&" +
                    "page=" + String.valueOf(page) +
                    "&output=json&callback=callback&save=yes";
            https://archive.org/advancedsearch.php?q=his&fl%5B%5D=title&sort%5B%5D=&sort%5B%5D=&sort%5B%5D=&rows=12&page=1&output=json&callback=callback&save=yes

            Log.d("link", buildQuery);

            return buildQuery;
        }
    }

    //
    private class DownloadTask extends AsyncTask<ResponseCallback, Void, String> {

        private OkHttpClient client;
        private Request request;
        private ResponseCallback callback;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            client = new OkHttpClient();
            request = new Request.Builder()
                    .url(buildQuery())
                    .build();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            if (response.isEmpty()) {
                callback.onFailure(new Exception("Response is empty"));
            } else {
                callback.onSuccess(response);
            }
        }

        @Override
        protected String doInBackground(ResponseCallback... params) {
            callback = params[0];
            try {
                return client.newCall(request).execute().body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }
        }

        protected String buildQuery() {
            String buildQuery = "";
            buildQuery = "https://archive.org/advancedsearch.php?" +
                        "q=collection%3A" + FREE_MOVIES + "+" +
                        "format%3A%5B512kb+MPEG4%5D&fl%5B%5D=" +
                        "identifier&fl%5B%5D=title&" +
                        "sort%5B%5D=titleSorter+asc&sort%5B%5D=&sort%5B%5D=&" +
                        "rows=" + String.valueOf(rows) + "&" +
                        "page=" + String.valueOf(page) +
                        "&output=json&callback=callback&save=yes";

            Log.d("link", buildQuery);

            return buildQuery;
        }
    }

    private class DownloadAcademicTask extends AsyncTask<ResponseCallback, Void, String> {

        private OkHttpClient client;
        private Request request;
        private ResponseCallback callback;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            client = new OkHttpClient();
            request = new Request.Builder()
                    .url(buildQuery())
                    .build();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            if (response.isEmpty()) {
                callback.onFailure(new Exception("Response is empty"));
            } else {
                callback.onSuccess(response);
            }
        }

        @Override
        protected String doInBackground(ResponseCallback... params) {
            callback = params[0];
            try {
                return client.newCall(request).execute().body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }
        }

        protected String buildQuery() {
            String buildQuery = "";
            buildQuery = "https://archive.org/advancedsearch.php?" +
                    "q=collection%3A" + ACADEMIC_FILMS + "+" +
                    "format%3A%5B512kb+MPEG4%5D&fl%5B%5D=" +
                    "identifier&fl%5B%5D=title&" +
                    "sort%5B%5D=titleSorter+asc&sort%5B%5D=&sort%5B%5D=&" +
                    "rows=" + String.valueOf(rows) + "&" +
                    "page=" + String.valueOf(page) +
                    "&output=json&callback=callback&save=yes";

            return buildQuery;
        }
    }

    private class DownloadEphmeraTask extends AsyncTask<ResponseCallback, Void, String> {

        private OkHttpClient client;
        private Request request;
        private ResponseCallback callback;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            client = new OkHttpClient();
            request = new Request.Builder()
                    .url(buildQuery())
                    .build();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            if (response.isEmpty()) {
                callback.onFailure(new Exception("Response is empty"));
            } else {
                callback.onSuccess(response);
            }
        }

        @Override
        protected String doInBackground(ResponseCallback... params) {
            callback = params[0];
            try {
                return client.newCall(request).execute().body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }
        }

        protected String buildQuery() {
            String buildQuery = "";
            buildQuery = "https://archive.org/advancedsearch.php?" +
                    "q=collection%3A" + EPHEMERA + "+" +
                    "format%3A%5B512kb+MPEG4%5D&fl%5B%5D=" +
                    "identifier&fl%5B%5D=title&" +
                    "sort%5B%5D=titleSorter+asc&sort%5B%5D=&sort%5B%5D=&" +
                    "rows=" + String.valueOf(rows) + "&" +
                    "page=" + String.valueOf(page) +
                    "&output=json&callback=callback&save=yes";

            return buildQuery;
        }
    }

    private class DownloadTelevisionTask extends AsyncTask<ResponseCallback, Void, String> {

        private OkHttpClient client;
        private Request request;
        private ResponseCallback callback;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            client = new OkHttpClient();
            request = new Request.Builder()
                    .url(buildQuery())
                    .build();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            if (response.isEmpty()) {
                callback.onFailure(new Exception("Response is empty"));
            } else {
                callback.onSuccess(response);
            }
        }

        @Override
        protected String doInBackground(ResponseCallback... params) {
            callback = params[0];
            try {
                return client.newCall(request).execute().body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }
        }

        protected String buildQuery() {
            String buildQuery = "";
            buildQuery = "https://archive.org/advancedsearch.php?" +
                    "q=collection%3A" + TELEVISION + "+" +
                    "format%3A%5B512kb+MPEG4%5D&fl%5B%5D=" +
                    "identifier&fl%5B%5D=title&" +
                    "sort%5B%5D=titleSorter+asc&sort%5B%5D=&sort%5B%5D=&" +
                    "rows=" + String.valueOf(rows) + "&" +
                    "page=" + String.valueOf(page) +
                    "&output=json&callback=callback&save=yes";

            return buildQuery;
        }
    }

    private class DownloadHorrorTask extends AsyncTask<ResponseCallback, Void, String> {

        private OkHttpClient client;
        private Request request;
        private ResponseCallback callback;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            client = new OkHttpClient();
            request = new Request.Builder()
                    .url(buildQuery())
                    .build();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            if (response.isEmpty()) {
                callback.onFailure(new Exception("Response is empty"));
            } else {
                callback.onSuccess(response);
            }
        }

        @Override
        protected String doInBackground(ResponseCallback... params) {
            callback = params[0];
            try {
                return client.newCall(request).execute().body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }
        }

        protected String buildQuery() {
            String buildQuery = "";

            buildQuery = "https://archive.org/advancedsearch.php?" +
                    "q=collection%3A" + HORROR_FILMS + "+" +
                    "format%3A%5B512kb+MPEG4%5D&fl%5B%5D=" +
                    "identifier&fl%5B%5D=title&" +
                    "sort%5B%5D=titleSorter+asc&sort%5B%5D=&sort%5B%5D=&" +
                    "rows=" + String.valueOf(rows) + "&" +
                    "page=" + String.valueOf(page2) +
                    "&output=json&callback=callback&save=yes";

            return buildQuery;
        }
    }

    private class DownloadSilentTask extends AsyncTask<ResponseCallback, Void, String> {

        private OkHttpClient client;
        private Request request;
        private ResponseCallback callback;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            client = new OkHttpClient();
            request = new Request.Builder()
                    .url(buildQuery())
                    .build();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            if (response.isEmpty()) {
                callback.onFailure(new Exception("Response is empty"));
            } else {
                callback.onSuccess(response);
            }
        }

        @Override
        protected String doInBackground(ResponseCallback... params) {
            callback = params[0];
            try {
                return client.newCall(request).execute().body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }
        }

        protected String buildQuery() {
            String buildQuery = "";

            buildQuery = "https://archive.org/advancedsearch.php?" +
                    "q=collection%3A" + SILENT_FILMS + "+" +
                    "format%3A%5B512kb+MPEG4%5D&fl%5B%5D=" +
                    "identifier&fl%5B%5D=title&" +
                    "sort%5B%5D=titleSorter+asc&sort%5B%5D=&sort%5B%5D=&" +
                    "rows=" + String.valueOf(rows) + "&" +
                    "page=" + String.valueOf(page) +
                    "&output=json&callback=callback&save=yes";

            return buildQuery;
        }
    }

    private class DownloadHomeTask extends AsyncTask<ResponseCallback, Void, String> {

        private OkHttpClient client;
        private Request request;
        private ResponseCallback callback;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            client = new OkHttpClient();
            request = new Request.Builder()
                    .url(buildQuery())
                    .build();
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            if (response.isEmpty()) {
                callback.onFailure(new Exception("Response is empty"));
            } else {
                callback.onSuccess(response);
            }
        }

        @Override
        protected String doInBackground(ResponseCallback... params) {
            callback = params[0];
            try {
                return client.newCall(request).execute().body().string();
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }
        }

        protected String buildQuery() {
            String buildQuery = "";

            buildQuery = "https://archive.org/advancedsearch.php?" +
                    "q=collection%3A" + HOME_MOVIES + "+" +
                    "format%3A%5B512kb+MPEG4%5D&fl%5B%5D=" +
                    "identifier&fl%5B%5D=title&" +
                    "sort%5B%5D=titleSorter+asc&sort%5B%5D=&sort%5B%5D=&" +
                    "rows=" + String.valueOf(rows) + "&" +
                    "page=" + String.valueOf(page3) +
                    "&output=json&callback=callback&save=yes";

            return buildQuery;
        }
    }


    private class FilesDownloadTask extends DownloadTask {
        private String identifier;

        public FilesDownloadTask(String identifier) {
            this.identifier = identifier;
        }

        @Override
        protected String buildQuery() {
            Log.d("buildQuery", ARCHIVE_DOWNLOAD + identifier + "/" + identifier + "_files.xml");
            return ARCHIVE_DOWNLOAD + identifier + "/" + identifier + "_files.xml";
        }
    }

    private class FilesDetailsTask extends DownloadAcademicTask {
        private String identifier;

        public FilesDetailsTask(String identifier) {
            this.identifier = identifier;
        }

        @Override
        protected String buildQuery() {
            return ARCHIVE_DETAILS + identifier + "/" + identifier + "_files.xml";
        }
    }

    private int page = 0;
    private int page2 = 2;
    private int page3 = 3;
    private int rows = 12;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void getSearchData(final ResponseCallback callback, String searchText) {
        ++page;
        downloadSearchTask = new DownloadSearchTask(searchText);
        downloadSearchTask.execute(callback);
    }

    public void getData(final ResponseCallback callback) {
        ++page;
        downloadTask = new DownloadTask();
        downloadTask.execute(callback);
    }

    public void getAcademicData(final ResponseCallback callback) {
        ++page;
        downloadAcademicTask = new DownloadAcademicTask();
        downloadAcademicTask.execute(callback);
    }

    public void getEphmeraData(final ResponseCallback callback) {
        ++page;
        downloadEphmeraTask = new DownloadEphmeraTask();
        downloadEphmeraTask.execute(callback);
    }

    public void getTelevisionData(final ResponseCallback callback) {
        ++page;
        downloadTelevisionTask = new DownloadTelevisionTask();
        downloadTelevisionTask.execute(callback);
    }

    public void getHorrorData(final ResponseCallback callback) {
//        ++page;
        downloadHhorrorTask = new DownloadHorrorTask();
        downloadHhorrorTask.execute(callback);
    }

    public void getSilentData(final ResponseCallback callback) {
        ++page;
        downloadSilentTask = new DownloadSilentTask();
        downloadSilentTask.execute(callback);
    }

    public void getHomeData(final ResponseCallback callback) {
//        ++page;
        downloadHomeTask = new DownloadHomeTask();
        downloadHomeTask.execute(callback);
    }

    public void getFilesData(String identifier, ResponseCallback callback) {
        new FilesDownloadTask(identifier).execute(callback);
    }

    public void getFileDetailsData(String identifier, ResponseCallback callback) {
        new FilesDetailsTask(identifier).execute(callback);
    }

    public void stopAsynctaskRunning() {
        if (downloadTask != null && downloadTask.getStatus() == AsyncTask.Status.RUNNING) {
            downloadTask.cancel(true);
        }

        if (downloadAcademicTask != null && downloadAcademicTask.getStatus() == AsyncTask.Status.RUNNING) {
            downloadAcademicTask.cancel(true);
        }

        if (downloadEphmeraTask != null && downloadEphmeraTask.getStatus() == AsyncTask.Status.RUNNING) {
            downloadEphmeraTask.cancel(true);
        }

        if (downloadTelevisionTask != null && downloadTelevisionTask.getStatus() == AsyncTask.Status.RUNNING) {
            downloadTelevisionTask.cancel(true);
        }
    }
}
